/**
 * 请补全JavaScript代码，该函数接收两个参数分别为旧版本、新版本，当新版本高于旧版本时表明需要更新，返回true，否则返回false。
  注意：
  1. 版本号格式均为"X.X.X"
  2. X∈[0,9]
  3. 当两个版本号相同时，不需要更新
 */

const _shouldUpdate = (oldVersion, newVersion) => {
  // 补全代码
  const olds = oldVersion.split('.');
  const news = newVersion.split('.');
  let num = 0
  for (let i = 0; i < 3; i++) {
    if (news[i] > olds[i] && i == num) {
      return true;
      break;
    } else if (news[i] == olds[i] && i == num) {
      num++;
      continue;
    } else if (news[i] < olds[i] && i == num) {
      return false;
      break;
    }
  }

}


_shouldUpdate('0.0.2', '0.0.0')
