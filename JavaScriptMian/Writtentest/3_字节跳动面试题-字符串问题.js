const a = 'abc';
const b = String('abc');
const c = new String('abc');


console.log(a === b); //true
console.log(a === c); //false
console.log(b === c); //false

// 使用new创建对象，是有原型链的，所以和a，b不同
console.log(a);
console.log(b);
console.log(c);

