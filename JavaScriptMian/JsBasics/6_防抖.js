/**
 * 手写防抖案例
 * 防抖：多次执行变成一次执行
 * fn是用户传入需要防抖的函数
 * delay是等待时间，这里设置为100毫秒
 */



function fangdou(fn, delay = 100) {
  //缓存一个定时器id
  let timer = 0;
  return function (...args) {
    /**
     * 这里返回的函数就是每次用户实际调用的防抖函数
     * 如果已经设定过定时器了，就清空上一次的定时器，开始一个新的定时器，延迟执行用户传入的方法。
     */
    if (timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      fn.apply(this, args);
    }, delay)
  }
}