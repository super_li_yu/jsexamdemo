/**
 * 处理数据
 * 要求：将'outQty','saleQty'格式为千分位,将数据处理完成后，按照原来格式输出。
 * 2022/11/16
 */

const res = {
  "code": 200,
  "message": null,
  "requestId": null,
  "data": {
    "outQty": [39539, 200422, 6906691],
    "saleQty": [39295, 199951, 6539669],
    "saleQualityRate": [96.03, 94.59, 90.53],
    "liveRate": [88.5500, 87.6500, 82.8300],
    "saleFeedWgtRatio": [2.77, 2.88, 3.04],
    "feedDay": [163, 166, 157],
    "age115": [194, 196, 208],
    "addWgtPerDay": [583.99, 542.92, 509.31],
    "fsa": [186.81, 165.26, 138.64]
  },
  "success": true,
  "params": null
}

/**
 * 处理数据2
 * 要求：将‘records’里的字段'currHerd'，和'salesQty'，进行数据格式化，并按照原数据格式进行输出。
 */
const res2 = {
  "code": 200,
  "message": null,
  "requestId": null,
  "data": {
    "current": "1",
    "total": "5",
    "size": "10",
    "pages": "1",
    "records": [
      {
        "columnId": "101015",
        "columnName": "猪BU-新好",
        "warZoneId": null,
        "warZoneName": null,
        "companyId": null,
        "companyName": null,
        "farmId": null,
        "farmName": null,
        "batchId": null,
        "batchCode": null,
        "raisingBatchNumber": 5,
        "exceedDeadBatchNumber": null,
        "currHerd": 2377,
        "salesQty": 11314,
        "ageDays": null
      },
      {
        "columnId": "101013",
        "columnName": "猪BU-新六",
        "warZoneId": null,
        "warZoneName": null,
        "companyId": null,
        "companyName": null,
        "farmId": null,
        "farmName": null,
        "batchId": null,
        "batchCode": null,
        "raisingBatchNumber": 18,
        "exceedDeadBatchNumber": null,
        "currHerd": 14260,
        "salesQty": 24956,
        "ageDays": null
      },
      {
        "columnId": "101024",
        "columnName": "猪BU-新海",
        "warZoneId": null,
        "warZoneName": null,
        "companyId": null,
        "companyName": null,
        "farmId": null,
        "farmName": null,
        "batchId": null,
        "batchCode": null,
        "raisingBatchNumber": 10,
        "exceedDeadBatchNumber": null,
        "currHerd": 8913,
        "salesQty": 23259,
        "ageDays": null
      },
      {
        "columnId": "101021",
        "columnName": "猪BU-新望",
        "warZoneId": null,
        "warZoneName": null,
        "companyId": null,
        "companyName": null,
        "farmId": null,
        "farmName": null,
        "batchId": null,
        "batchCode": null,
        "raisingBatchNumber": 13,
        "exceedDeadBatchNumber": null,
        "currHerd": 16621,
        "salesQty": 17565,
        "ageDays": null
      },
      {
        "columnId": "101011",
        "columnName": "饲料BU",
        "warZoneId": null,
        "warZoneName": null,
        "companyId": null,
        "companyName": null,
        "farmId": null,
        "farmName": null,
        "batchId": null,
        "batchCode": null,
        "raisingBatchNumber": 8,
        "exceedDeadBatchNumber": null,
        "currHerd": 5600,
        "salesQty": 2886,
        "ageDays": null
      }
    ]
  },
  "success": true,
  "params": null
}

/**
 * 千分位显示
 * val: 需要格式化的数字
 */
const toThousand = (val) => {
  if (Number.isNaN(val) || val === undefined || val === null || val === 'NaN' || val === 'Infinity' || val === '--') {
    return '--';
  }
  try {
    const left = val < 0;
    const n = Math.abs(val);
    const str = n.toString().replace(/\B(?=(\d{3})+\b)/g, ',');
    return left ? `-${str}` : str;
  } catch (e) {
    return '--';
  }
};

//res处理核心
for (var item in res.data) {
  if (item === 'outQty' || item === 'saleQty') {
    for (let i = 0; i < res.data[item].length; i++) {
      res.data[item][i] = toThousand(res.data[item][i]);
    }
  }
}


// console.log(res)
//处理结果
/**
 * {
  outQty: [ '39,539', '200,422', '6,906,691' ],
  saleQty: [ '39,295', '199,951', '6,539,669' ],
  saleQualityRate: [ 96.03, 94.59, 90.53 ],
  liveRate: [ 88.55, 87.65, 82.83 ],
  saleFeedWgtRatio: [ 2.77, 2.88, 3.04 ],
  feedDay: [ 163, 166, 157 ],
  age115: [ 194, 196, 208 ],
  addWgtPerDay: [ 583.99, 542.92, 509.31 ],
  fsa: [ 186.81, 165.26, 138.64 ]
}
 */

