function getTime() {
  let data = new Date(); //初始化dta对象
  let year = data.getFullYear(); //获取当前year
  let month = data.getMonth() + 1; //获取当前月份
  let date = data.getDay(); //获取当前日期
  let hour = data.getHours(); //获取当前时间
  let day = data.getDay(); //获取当前是星期几
  //定义一个星期数组，方便下面进行展示星期
  let dayArray = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']
  let days = dayArray[day];
  //返回结果
  return `${year}年-${month}月-${date}日-${hour}时-${days}`
}

console.log(getTime());
