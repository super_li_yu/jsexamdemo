var str = "hello"
var arr = [0, 1, 2]
//数据类型的判断方法一
function typeofmethod(obj) {
  let res = Object.prototype.toString.call(obj).split(' ')[1];
  res = res.slice(0, res.length - 1).toLowerCase();
  return res;
}

//数据类型的判断方法二
function typeofmethod2(obj) {
  return Object.prototype.toString.call(obj).slice(8, - 1).toLowerCase();
}


console.log("第二种判断方法:" + typeofmethod2(str));
console.log("第一种判断方法:" + typeofmethod(str));

