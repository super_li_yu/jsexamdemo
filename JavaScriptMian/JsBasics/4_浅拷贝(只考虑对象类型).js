/**
 * 浅拷贝，只考虑引用类型
 */

function shllowCopy(obj) {
  //先对传入的实参进行判断，是否为对象类型，不是就退出函数
  if (typeof obj != 'object') {
    return
  }
  //根据传入的实参进行判断，实例好新的对象类型(数组/对象)
  let newObj = obj instanceof Array ? [] : {}
  for (let key in obj) {
    // for in 语句循环遍历对象的属性，每次迭代返回一个键, 键用来访问键的值。
    // for of语句循环遍历可迭代对象的值。可以循环遍历数组，字符串，映射，节点列表。
    //查看两者区别:http://t.csdn.cn/v6y8W
    if (obj.hasOwnProperty(key)) {
      //对象.hasOwnProperty(属性名)：用来判断在对象中查找是否有该属性，如果有返回true，否则返回false,该方法不会到原型链中查找。
      //这步用来判断对象中是否有该键值，如果有的话就直接复制该属性。
      newObj[key] = obj[key]
    }
  }
  //返回结果
  return newObj
}


var person = {
  name: '抗争的小青年',
  age: 22,
  hobby: '热爱编程'
}

console.info(shllowCopy(person))