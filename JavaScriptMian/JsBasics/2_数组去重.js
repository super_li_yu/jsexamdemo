//定义数组
var arr = [1, 2, 3, 3, 4, 4, 4, 5];



//ES5实现
function unique(arr) {
  var res = arr.filter(function (item, index, array) {
    return array.indexOf(item) == index;
  })
  return res
}

//ES6实现
var newarr = arr => [...new Set(arr)];


console.log(unique(arr));
// console.log(newarr);
