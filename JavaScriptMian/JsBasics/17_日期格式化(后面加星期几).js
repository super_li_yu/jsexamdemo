function processRankTime(time) {
  const date = new Date(time);
  if (!time || date.toString == 'Invalid Date')
    return '-';
  const weekdays = ['一', '二', '三', '四', '五', '六', '日'];
  return `${time}（周${weekdays[date.getDay() - 1]}）`;
}

console.log(processRankTime('2022-11-28 17:25:24'));


/**
 * 使用场景：传过一个字符串（日期），返回'日期-(周x)'
 * 1.将传进来的time重新new 一个data对象。
 * 2.判断data是否是符合，对其值的合法性进行判断，如果不合法就返回'-'。
 * 3.定义一个存放周数据的数组weekdays。
 * 4.使用模板字符串拼接上数据进行返回。
 */