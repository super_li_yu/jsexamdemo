/**
 * 手写一个ajax请求
 * 借助原生xttprequest
 */

function ajaxText(url) {
  //1. 创建对象
  let xhr = new XMLHttpRequest();
  //2. 创建http请求
  xhr.open('GET', rul, true);
  //3. 设置状态监听函数
  xhr.onreadystatechange = function () {
    if (this.readyState !== 4) return
    if (this.status === 200) {
      handle(this.response)
    } else {
      console.error(this.statusText);
    }
  }

  //设置请求失败时候的监听函数
  xhr.onerror = function () {
    console.error(this.statusText);
  }
  //设置请求头信息
  xhr.responseType = 'json'
  xhr.setRequestHeader('Accept', 'application/json')
  //发送请求
  xhr.send();
}