/**
 * 数组扁平化就是将[1,[2,[3]]]这种多层的数组拍平成一层[1,2,3]。使用array.protope.flat可以直接将多层数组拍平成一层。
 * flat后面有参数，里面是几就是扁平化几层，像这种情况就是需要扁平化两层。
 */

var arr = [1, [2, [3]]];
console.log(`这是调用原生方法进行数组扁平化操作：${arr.flat(2)}`);
console.log(arr.flat(1));
/**
 * 接下来是实现flat方法
 * 递归
 */

function flatten(arr) {
  //定义结果集
  var res = [];
  //对每一项进行一次遍历
  for (var i = 0; i < arr.length; i++) {
    //查看该项是否为数组，如果是，就对其进行递归,不是数组就直接添加到结果集中并返回
    if (Array.isArray(arr[i])) {
      res = res.concat(flatten(arr[i]))
    } else {
      res.push(arr[i])
    }
  }
  return res;
}

console.log(`这是调用自己写方法进行数组扁平化操作：${flatten(arr)}`);
console.log(flatten(arr));