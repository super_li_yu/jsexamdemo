/**
 * 手写节流案例
 * 节流：在一段时间内，多次点击变成隔规定时间执行一次。
 * fn是用户传入需要节流的函数
 * delay是等待时间
 */

function throwtloe(fn, delay) {
  //定义一个标记
  let flag = true;
  //返回一个函数
  return () => {
    //如果flag = false 直接返回
    if (!flag) return;
    //这时flag = true 执行下一步，先将flag 置为 false
    flag = false;
    //定义一个计时器，并在规定时间内执行函数
    timer = setTimeout(() => {
      fn();
      flag = true;
    }, delay)
  }
}