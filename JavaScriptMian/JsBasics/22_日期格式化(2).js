/**
 * 日期格式化方法
 * @param {*} a 入参'20xx-xx-xx'
 * @returns 格式化数据 '20xx年xx月xx日'
 */


function handleDate(a) {
  let pattern = /^20\d{2}-\d[1-9]-\d[1-9]$/g;
  if (pattern.test(a)) {
    const newDate = a.split('-');
    return newDate[0] + '年' + newDate[1] + '月' + newDate[2] + '日';
  } else {
    return a
  }
}

const result = handleDate('2023-06-12');
console.log(result);