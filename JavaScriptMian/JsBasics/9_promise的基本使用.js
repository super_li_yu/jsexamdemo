// 1.prmose的一个基本使用
function timeout(ms) {
  return new Promise((resolve, resject) => {
    setTimeout(resolve, ms, 'done');
  })
}

timeout(100).then(value => {
  console.log(value);
})

console.log('———————————er——————————');//同步方法，优先执行
/**
 * 这个代码中，使用timeout方法返回一个promise实例，表示一段时间以后才会发生的结果
 * ，Promise实例的状态变为resolved就会触发then方法绑定的回调函数。
 */

let promise = new Promise((resolve, resject) => {
  console.log('Promise');
  resolve();
});

promise.then(function () {
  console.log('resolved');
});

console.log('Hi!');//同步方法，优先执行



/**
 * 异步加载图片的例子
 */

function loadImageAsync(url) {
  return new Promise((resolve, resject) => {
    const img = new Image();

    img.onload = function () {
      resolve(img);
    }
    img.onerror = function () {
      resject(new Error('未能加载图片' + url));
    }

    img.src = url
  });
}


