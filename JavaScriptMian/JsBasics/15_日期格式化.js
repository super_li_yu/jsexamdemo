/**
 * 日期格式化函数
 */

function parseTime(time, pattern) {
  //先判断输入的格式是否正确，arguments.length===0说明输入为空，time为空时，!time为真
  if (arguments.length === 0 || !time) {
    return null
  }
  //这里如果pattern有格式，就按着格式的来，否则就使用默认的'{y}-{m}-{d} {h}:{i}:{s}'
  const format = pattern || '{y}-{m}-{d} {h}:{i}:{s}'
  //定义日期变量，一会用来接收"time"
  let date
  //如果是2022这样的就转成整数
  if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
    time = parseInt(time);
  } else if (typeof time === 'string') {
    //这种的就不同了，可能是2022-10-9,就把'-'替换为'/'
    time = time.replace(new RegExp(/-/gm), '/').replace('T', ' ').replace(new RegExp(/\.[\d]{3}/gm), '')
  } if ((typeof time === 'number') && (time.toString().length === 10)) {
    time = time * 1000
  }
  //定义一个日期对象，并对里面的属性进行赋值
  date = new Date(time);
  const fromatObj = {
    y: date.getFullYear(),
    m: date.getMonth(),
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  //生成时间字符串，并按照里面的格式，对数据进行替换
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = fromatObj[key]
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value] }
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}
