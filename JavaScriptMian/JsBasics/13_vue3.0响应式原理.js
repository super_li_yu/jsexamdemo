/**
 * Vue3.0响应式的原理：基于proxy
 * proxy是es6新特性，为了对目标的作用主要是通过handler对象中的拦截方法拦截目标对象target的某些行为
 * （如属性查找、赋值、枚举、函数调用等）。
 * 
 */

const handler = {
  get: function (obj, prop) {
    return prop in obj ? obj[prop] : '没有此水果';
  }
}

const foot = new Proxy({}, handler)
foot.apple = '苹果';
foot.banana = '香蕉';


console.log(foot.apple, foot.banana);
console.log('pig' in foot, foot.pig);