const arr1 = [1, 2, 3, 4];
const arr2 = [5, 6, 7, 8];

// var arryList = [];

// arryList.concat(arr1).concat(arr2);

const arryList = arr1.concat(arr2);

console.log('arryList', arryList);
console.log('arr1', arr1);
console.log('arr2', arr2);

//原数组不变，创建新数组，只复制对象的引用。

//嵌套数组也可以使用
const arr3 = [[9], 10, 11];
//三个数组相互嵌套
const arryList1 = arr3.concat(arr1, arr2);

console.log(arryList1);