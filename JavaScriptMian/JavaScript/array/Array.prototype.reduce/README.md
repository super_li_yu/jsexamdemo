# Array.prototype.reduce

reduce() 方法对数组中的每个元素按序执行一个由您提供的 reducer 函数，每一次运行 reducer 会将先前元素的计算结果作为参数传入，最后将其结果汇总为单个返回值。

# 语法
```js
// 箭头函数
reduce((previousValue, currentValue) => { /* … */ } )
reduce((previousValue, currentValue, currentIndex) => { /* … */ } )
reduce((previousValue, currentValue, currentIndex, array) => { /* … */ } )

reduce((previousValue, currentValue) => { /* … */ } , initialValue)
reduce((previousValue, currentValue, currentIndex) => { /* … */ } , initialValue)
reduce((previousValue, currentValue, currentIndex, array) => { /* … */ }, initialValue)

// 回调函数
reduce(callbackFn)
reduce(callbackFn, initialValue)

// 内联回调函数
reduce(function(previousValue, currentValue) { /* … */ })
reduce(function(previousValue, currentValue, currentIndex) { /* … */ })
reduce(function(previousValue, currentValue, currentIndex, array) { /* … */ })

reduce(function(previousValue, currentValue) { /* … */ }, initialValue)
reduce(function(previousValue, currentValue, currentIndex) { /* … */ }, initialValue)
reduce(function(previousValue, currentValue, currentIndex, array) { /* … */ }, initialValue)

```

# 参数
callbackFn
- previousValue:上一次调用callbackFn时的返回值。在第一次调用时，若指定了初始值 initialValue，其值则为 initialValue，否则为数组索引为 0 的元素 array[0]。
- currentValue:数组中正在处理的元素。在第一次调用时，若指定了初始值`initialValue`，其值则为数组索引中为0的元素`array[0]`,否则为`array[1]`。
- currentIndex:数组中正在处理的元素的索引。若指定了初始值`initalValue`，则起始索引号为0，否则从索引1起始。
- array:用于遍历的数组。
- initialValue(非必须):作为第一次调用 callback 函数时参数 `previousValue` 的值。若指定了初始值 `initialValue`，则 `currentValue` 则将使用数组第一个元素；否则 `previousValue` 将使用数组第一个元素，而 `currentValue` 将使用数组第二个元素。

> callbackfn 应是一个接受四个参数的函数，reduce 对于数组中第一个元素之后的每一个元素，按升序各调用一次回调函数。

- callbackfn 被调用时会传入四个参数：

- previousValue（前一次调用 callbackfn 得到的返回值）
- currentValue（数组中正在处理的元素）
- currentIndex（数组中正在处理的元素的索引）

> 回调函数第一次执行时，previousValue,和currentValue的值是有不同情况的。
> - 如果调用 reduce() 时提供了 initialValue，previousValue 取值则为 initialValue，currentValue 则取数组中的第一个值。
> - 如果没有提供 initialValue，那么 previousValue 取数组中的第一个值，currentValue 取数组中的第 二个值。