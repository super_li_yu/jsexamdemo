//目标数组
const array = [1, 2, 3, 4, 5, 6, 7];
var sum = 0;

/**
 * 求和，并定义初始值为0，进行累加
 */
sum = array.reduce((pre, cur) => {
  return pre + cur;
}, 0);

console.log(`sum的值为:${sum}`)