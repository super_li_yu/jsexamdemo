# Vue2.0不能监听数据和对象变化的原因

1. 不要把Vue2.0不能监听数组的原因怪罪到`Object.defiendProperty`身上。

> Object.defiendProperty是可以监听到数组下标的变化的，**主要原因是从用户体验和提升的效率不划算而弃用。**

2. `Object.defineProperty`在可以对数组元素进行监听，通过索引访问或者设置对应元素的值时，可以出发对应的`getter`和`setter`方法。通过`push`和`unshift`会增加索引，对应新增加的属性，需要再次初始化才能被监听到变化。

   

# Vue2.0和Vue3.0在数据代理上有何不同

**Object.defineProperty** :vs: **Proxy**

1. **Object.defineProperty只能劫持对象的属性，而Proxy是直接代理对象。**

> 由于 `Object.defineProperty` 只能对属性进行劫持，需要遍历对象的每个属性，如果属性值也是对象，则需要深度遍历。而 `Proxy` 直接代理对象，不需要遍历操作。

2. **Object.defineProperty对新增属性需要手动进行Observe**

> 由于 `Object.defineProperty` 劫持的是对象的属性，所以新增属性时，需要重新遍历对象，对其新增属性再使用 `Object.defineProperty` 进行劫持。