# async函数

## 介绍
一种异步处理的解决方案,async返回的是一个Promise对象，可以使用`then`方法添加回调函数，实现链式调用.

一旦遇到await会先返回，等到异步操作完成，再接着执行函数体后面的语句。

## 语法
async函数返回一个Promise对象。async函数内部`return`语句返回的值，会成为`then`方法回调函数的参数。

```js
async function f(){
  return 'hello world';
}

fn().then(v=>console.log(v))
```
## 函数执行规则

async函数返回的 Promise 对象，`必须等到内部所有await命令后面的 Promise 对象执行完`，才会发生状态改变，除非遇到return语句或者抛出错误。也就是说，只有async函数内部的异步操作执行完，才会执行then方法指定的回调函数。

> 这里要注意，async和await的使用场景不是等待异步函数那么简单，等待异步是没错，但await是需要等待的是promise，也就是说若在函数中有返回promise，才需要await等待，await也能等到，否则就没必要去使用async和await.所以使用await代替setTimeout是完全没有必要的。
> async函数返回的是一个promise对象，并且患有状态(state)和结果(result)，若函数有返回值，那么内部会调用Promise.resolve()方法把它转化成一个Promise对象作为返回，若执行失败，则会调用Promise.reject()返回一个Promise对象。

## await
await关键字只能放到async函数里面，await是等待的意思。
### await在等什么？
await不仅可以等Promise对象，还可以等待任何表达式，所以await后面实际是可以直接接普通函数或者直接量等待，不过更多的是放一个返回Promise对象的表达式。

```javascript
function getDetial() {
  return 'something';
}

async function testAsync() {
  //返回一个成功的Promise状态，并携带参数'done'
  return Promise.resolve('done');
}

async function test() {
  const v1 = await getDetial();
  const v2 = await testAsync();
  console.log(v1, v2);
}

test();
```
> 当执行test()函数后，返回`something done`

这里分两种情况
1. 等待到的不是Promise
2. 等待到的只是普通的返回值

若是1情况，await就会阻塞后面的代码，等着Promise触发resolve，然后得到resolve的值，作为await表达式的运算结果。

若是2情况，await表达式的返回结构就是上个函数返回的值。

### 使用setTimeout和使用await的对比
```javaScript
/**
 * 使用setTimeout模拟耗时的异步操作
 * 延时1s
 */

function takeLongTime() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('long_time_value')
    }, 1000)
  })
}

//打印输出
takeLongTime().then(val => {
  console.log(`延迟1s后打印输出:${val}`);
})


/**
 * 改为用async/await后
 * 延迟1s
 */

function takeLongTime_E() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('long_time_value_E');
    }, 1000)
  })
}

async function test_E() {
  let v = await takeLongTime_E();
  console.log(`延迟两秒后输出${v}`);
}
```
> 但是用await后，可以使用`v`变量，直接将`takeLongTime_E`的返回结果拿取到，并返回给`v`.

### 使用await的优势
await的优势在于处理then链，使得代码从链式调用变成了同步调用，代码更加美观。


