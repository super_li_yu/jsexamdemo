const fs = require('fs')


async function rendFile() {
  await fs.readFile('../file/Apprenticeship table.txt', 'utf-8', (err, res) => {
    if (err) {
      return console.log(`文件读取失败:${err.message}`);
    } else {
      console.log(res);
    }
  })
}

//使用async控制异步
rendFile();


console.log('---------------------------------------')
//使用setTimeout控制异步

setTimeout(() => {
  fs.readFile('../file/exitDetail.txt', 'utf-8', (err, res) => {
    if (err) {
      return console.log(`文件读取失败:${err.message}`);
    } else {
      console.log(res);
    }
  }, 1500)
})


