/**
 * 编写一个函数
 * 2s之后让数值乘2
 */

function double(val) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(val * 2);
    }, 2000)
  })
}

async function retResult() {
  //使用await关键字等待double函数返回Promise的resolve结果
  let result = await double(20);
  let result1 = await double(30);
  let result2 = await double(40);
  let result3 = await double(60);
  console.log(`延迟2秒输出:${result}`);
  console.log(`延迟4秒后输出:${result1}`);
  console.log(`延迟6秒后输出:${result2}`);
  console.log(`延迟8秒后输出:${result3}`);
}

//执行异步方法
retResult();