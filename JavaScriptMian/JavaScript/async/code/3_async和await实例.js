function getDetial() {
  return 'something';
}

async function testAsync() {
  //返回一个成功的Promise状态，并携带参数'done'
  return Promise.resolve('done');
}

async function test() {
  const v1 = await getDetial();
  const v2 = await testAsync();
  console.log(v1, v2);
}

test();