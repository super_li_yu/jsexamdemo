//async对象状态的变化
/**
 * 
 * @param {*} url 
 * @returns 返回"ECMAScript 2017 Language Specification"
 * 由于不在浏览器环境下运行,fetch无法使用
 */
async function getTitel(url) {
  //fetch is not defined
  let response = await fetch(url);
  let html = await response.text();
  return html.match(/<title>([\s\S]+)<\/title>/i)[1];
}

getTitel('https://tc39.github.io/ecma262/').then(console.log)