/**
 * 使用setTimeout模拟耗时的异步操作
 * 延时1s
 */

function takeLongTime() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('long_time_value')
    }, 1000)
  })
}

//打印输出
takeLongTime().then(val => {
  console.log(`延迟1s后打印输出:${val}`);
})


/**
 * 改为用async/await后
 * 延迟1s
 */

function takeLongTime_E() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('long_time_value_E');
    }, 1000)
  })
}

async function test_E() {
  let v = await takeLongTime_E();
  console.log(`延迟两秒后输出${v}`);
}