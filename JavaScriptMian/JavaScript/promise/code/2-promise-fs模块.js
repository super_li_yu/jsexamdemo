const fs = require('fs');
//普通的封装
fs.readFile('../resource/content.txt', 'utf-8', (err, data) => {
  //如果出错，就抛出错误
  if (err) throw err;
  console.log(data)
})

//Promise的方式对代码进行封装
let p = new Promise((resolve, reject) => {
  fs.readFile('../resource/content.txt', 'utf-8', (err, data) => {
    //如果出错，就抛出错误
    if (err) reject(err);
    //成功
    resolve(data);
  })
}).then((data) => {
  console.log(data);
}, (err) => {
  console.log(err);
})