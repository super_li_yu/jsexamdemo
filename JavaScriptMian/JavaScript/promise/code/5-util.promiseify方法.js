/**
 * util.promisify方法
 * node中的util模块
 * 作用：将原本需要通过传入回调函数来实现，用Promise的.then方法来调用，从而实现逻辑上的的同步操作。
 * promisefy是个函数，参数里面传入函数，Promisefy的返回值也是个函数，调用这个函数，这个函数返回的就是promise对象。
 */
const util = require('util');
const fs = require('fs')
let myReadFile = util.promisify(fs.readFile);
myReadFile('../resource/content.txt', 'utf-8').then(value => {
  console.log(value);
})