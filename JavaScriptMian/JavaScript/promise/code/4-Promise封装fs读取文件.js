/**
 * 封装一个函数 myReadFile 读取文件内容
 * 参数：path 文件路径
 * 返回：promise对象
 */



function myReadFile(path) {
  return new Promise((resolve, reject) => {
    require('fs').readFile(path, 'utf-8', (err, data) => {
      //判断
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    })
  })
}

myReadFile('../resource/content.txt').then((data) => {
  console.log(data);
}, (err) => {
  console.log(err);
})