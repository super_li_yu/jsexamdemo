let p = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('ok')
  }, 1000)
});

p.then(value => {
  console.log(111)
  //返回一个padding状态的promise的回调对象
  return new Promise(() => { })
}).then(value => {
  console.log(222);
}).then(value => {
  console.log(333);
}).catch(reson => {
  console.log(reson);
})