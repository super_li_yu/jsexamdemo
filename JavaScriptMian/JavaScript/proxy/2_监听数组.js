let subject = ['高等数学']
let handler = {
  get(obj, key) {
    console.log(`访问了${key}属性`);
    return key in obj ? obj[key] : '没有这门学科'
  },
  set(obj, key, val) {
    console.log(`${key}属性被改成了${val}`);
    obj[key] = val;
    //set方法成功时应该返回true,否则会报错
    return true
  }
}


let proxyObj = new Proxy(subject, handler)

console.log(proxyObj[1]);
proxyObj[0] = '大学物理'
console.log(proxyObj);

proxyObj.push('线性代数')
console.log(proxyObj);