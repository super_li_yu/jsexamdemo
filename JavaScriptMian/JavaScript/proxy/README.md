# Vue3.0_Proxy

## 基本参数

1. `target`：要使用proxy包装的目标对象(可以是任意类型的对象，包括原生数组函数，甚至另外一个代理).
2. `handler`：一个通常以函数作为属性的对象，各属性中的函数分别定义了在执行各种操作时代理p行为.

```js
const p = new Proxy(target,handler)
```

## 小例子
```js
let preson = {
  age: 18,
  name: 'liyu'
}

//定义handle对象
let hander = {
  get(obj, key) {
    console.log(`访问了${obj}属性`);
    return key in obj ? obj[key] : '属性不存在'
  },
  set(obj, key, val) {
    console.log(`${key}的属性被修改成了${val}`);
    obj[key] = val
    return true
  }
}

//把handler对象传入proxy
let proxyObj = new Proxy(preson, hander)

//一次对全部属性进行监听
//测试get能否可以拦截成功
console.log(proxyObj.age);
console.log(proxyObj.name);


//测试set方法能否拦截成功
proxyObj.age = 22;
console.log(proxyObj.age);


//对于对象的新增类型，不需要手动监听
console.log(proxyObj.user);

```

## 区别
vue.js 1.x和vue.js 2.x内部都是通过Object.defineProperty这个API去劫持数据的getter和setter，具体是这样的：
```js
Object.defineProperty(data,'a',{
  get(){
    //track
  },
  set(){
    //trigger
  }
})
```
> 有一定的缺陷，key无法提前知道

vue.js 3.0使用了Proxy API做数据坚持，它的内部
```js
observed = new Proxy(data,{
  get(){
    //track
  },
  set(){
    //trigger
  }
})

```