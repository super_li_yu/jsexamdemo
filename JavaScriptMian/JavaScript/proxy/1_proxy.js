let preson = {
  age: 18,
  name: 'liyu'
}

//定义handle对象
let hander = {
  get(obj, key) {
    console.log(`访问了${obj}属性`);
    return key in obj ? obj[key] : '属性不存在'
  },
  set(obj, key, val) {
    console.log(`${key}的属性被修改成了${val}`);
    obj[key] = val
    return true
  }
}

//把handler对象传入proxy
let proxyObj = new Proxy(preson, hander)

//一次对全部属性进行监听
//测试get能否可以拦截成功
console.log(proxyObj.age);
console.log(proxyObj.name);


//测试set方法能否拦截成功
proxyObj.age = 22;
console.log(proxyObj.age);


//对于对象的新增类型，不需要手动监听
console.log(proxyObj.user);
