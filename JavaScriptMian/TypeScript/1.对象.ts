/**
 * 复杂数据类型-对象
 * 通过接口定义对象属性类型
 */
interface Preson {
  name: string,
  age: number,
  class?: number,//？代表可有可无的属性 
  sayHi: () => string
}


var xiam: Preson = {
  name: "小米",
  age: 37,
  sayHi() {
    console.log('你好');
    return '您好'
  },
}

console.log(xiam.sayHi());
