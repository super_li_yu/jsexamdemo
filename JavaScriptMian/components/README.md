# 概况
优质组件学习，记录。
# tree.vue

## 基于uni-app的日历选择器，支持自定义范围/集合选择，支持单选、多选

## 安装方式
本组件符合[easycom](https://uniapp.dcloud.io/collocation/pages?id=easycom)规范，请使用[newhope初始化命令工具](https://git.oak.net.cn/nh-web/base/web-init/blob/master/README.md)进行安装导入项目，在页面`template`中即可直接使用，无需在页面中`import`和注册`components`。

 - 在项目的`package.json`中的`uni_modules`节点下新增`nhu-tree`

```
"uni_modules": {
    "nhu-tree": ">=1.0.2"
}
```

 - 使用`newhope`命令安装到`uni_modules`文件夹下

```
newhope i
```

## 基本用法

在 ``template`` 中使用组件

```html
<nhu-tree ref="nhuTree" :value="orgTree.value" :search="orgTree.search"
			:treeData="orgTree.list" :labelKey="orgTree.labelKey" :valueKey="orgTree.valueKey"
			:multiple="orgTree.multiple" @confirm="orgTreeConfirm"></nhu-tree>
```
```data
orgTree: {
		list: [{
			id: '1',
			name: '2',
			type: 'a',
			children: [{
				id: '3',
				name: '4',
				type: 'b',
				children: []
			},{
				id: '5',
				name: '6',
				type: 'b',
				children: []
			}],
			open: true
		}],
		multiple: true,
		valueKey: 'id',
		labelKey: 'name',
		search: false,
		value: ''
	}
```
```js
this.$ref.nhuTree.open()   // 打开
this.$ref.nhuTree.close()  // 关闭
orgTreeConfirm(e){
	// 返回选中list
}
```

## 参数说明

|属性名			|类型				|默认值		|说明																|	
|:-:			|:-:				|:-:		|:-:																|
|list			|Array				|[]			|{id:'', name:'', children:[]下级 , open:false是否展开,type:''}		|
|value			|Array, String		|[]			|默认值Id(如需要配合展开，需在list主机配置open:true)					|
|search			|Boolean			|true		|是否显示搜索框														|
|header			|Boolean			|true		|是否显示header														|
|multiple		|Boolean			|false		|true多选  false单选													|
|title			|String				|''			|选自器标题															|
|placeHolder	|String				|''			|占位文字															|
|valueKey		|String				|'id'		|主键键名															|
|labelKey		|String				|'name'		|展示的键名															|
|pageSize		|Number, Boolean	|50			|指定范围生效：选中日期												|


## 方法

|方法称名	|说明			|参数	|
|:-:		|:-:			|:-:	|
|open		|打开弹出层		|-		|
|close		|关闭弹出层		|-		|

## 事件


|事件称名	|说明			|返回值	|
|:-:		|:-:			|:-:	|
|@confirm	|确认回调		|{id,name,parent,seniorIds父级id,type...} PS:根据type判断层级	|
|@change	|回调			|同上														|