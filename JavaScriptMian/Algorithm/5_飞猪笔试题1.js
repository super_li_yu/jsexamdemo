/**
 * 按type将如上数据结构归类到一个对象结构中，并给每个type内的数据，按value值的大小，正序排序，如上面的数组输出如下结果
 * 注意：type的值数量不固定，实现方法请考虑通用性
 * 
 */

const res = {
  a: [
    { value: 132, lable: 4, type: 'a' },
    { value: 532, lable: 1, type: 'a' },
    { value: 2462, lable: 9, type: 'a' }
  ],
  b: [
    { value: 132, lable: 24, type: 'b' },
    { value: 432, lable: 13, type: 'b' },
    { value: 1812, lable: 5, type: 'b' },
  ],
  c: [
    { value: 1932, lable: 8, type: 'c' }
  ]
}

const arr = [
  { value: 1232, lable: 3, type: 'one' },
  { value: 532, lable: 1, type: 'two' },
  { value: 132, lable: 24, type: 'three' },
  { value: 432, lable: 13, type: 'two' },
  { value: 1812, lable: 5, type: 'two' },
  { value: 1932, lable: 8, type: 'three' },
  { value: 132, lable: 4, type: 'one' },
  { value: 2462, lable: 9, type: 'one' }
]

const processArr = (arr) => {
  let res = {};
  let maps = new Map();
  arr.forEach(item => {
    console.log(item);
    if (maps.has(item.type)) {

    } else {
      maps.set(item.type, 1);
    }
    console.log(maps);
  })
  return res;
}


console.log(JSON.stringify(processArr(arr)))