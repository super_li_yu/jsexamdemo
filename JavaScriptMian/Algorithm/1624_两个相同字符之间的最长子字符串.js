// 给你一个字符串 s，请你返回 两个相同字符之间的最长子字符串的长度 ，计算长度时不含这两个字符。如果不存在这样的子字符串，返回 -1 。

// 子字符串 是字符串中的一个连续字符序列。


// 示例 1：
// 输入：s = "aa"
// 输出：0
// 解释：最优的子字符串是两个 'a' 之间的空子字符串。

// 示例 2：
// 输入：s = "abca"
// 输出：2
// 解释：最优的子字符串是 "bc" 。

// 示例 3：
// 输入：s = "cbzxy"
// 输出：-1
// 解释：s 中不存在出现出现两次的字符，所以返回 -1 。

// 示例 4：
// 输入：s = "cabbac"
// 输出：4
// 解释：最优的子字符串是 "abba" ，其他的非最优解包括 "bb" 和 "" 。

/**
 * @param {string} s
 * @return {number}
 */
var maxLengthBetweenEqualCharacters = function (s) {
  const map = {};
  const tracn = {};
  let max = -1;
  for (let i in s) {
    map[i] = (map[i] || 0) + 1;
  }
  for (let i = 0; i < s.length; i++) {
    if (map[s[i]] == 1) {
      continue;
    } else {
      tracn[s[i]] = s.lastIndexOf(s[i]) - s.indexOf(s[i]) - 1;
    }
  }
  for (let i in tracn) {
    max = Math.max(max, tracn[i]);
  }
  return max;
};
maxLengthBetweenEqualCharacters('cabbac')
