// url参数解析http://xxx.com/yyy?a=1&b=2&c=3   返回 {a:1,b:2,c:3}

const url = 'http://xxx.com/yyy?a=1&b=2&c=3';
function dosome(str) {
  var res = {};
  var params = str.split('?')[1].split('&');
  for (let i = 0; i < params.length; i++) {
    res[params[i].split('=')[0]] = params[i].split('=')[1];
  }
  return res;
}

console.log(dosome(url))
