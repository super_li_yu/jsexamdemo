/**
 * 编写一个JavaScript函数 parseQueryString，把URL参数解析为一个对象
 * 输入：const url = "?name=feizhu&from=alibaba&job=frontend&extraInfo=%7B%22a%22%3A%22b%22%2C%22c%22%3A%22d%22%7D"
        const params= parseQueryString(url)
 * 输出：
 */

const res = {
  name: "feizhu",
  from: 'alibaba',
  job: 'frontend',
  extraInfo: { a: 'b', c: 'd' }
}

// -----作答
const parseQueryString = (target) => {
  //获取query
  var str = target.split('?')[1];
  var result = {};
  var temp = str.split('&');
  temp.forEach((v) => {
    let str = v.split('=')[1]
    console.log(str);
    if (str.indexOf('%') === -1) {
      result[v.split('=')[0]] = v.split('=')[1];
    } else {
      result[v.split('=')[0]] = JSON.parse(decodeURIComponent(v.split('=')[1]))
    }
  });
  return result;
}

const url = "?name=feizhu&from=alibaba&job=frontend&extraInfo=%7B%22a%22%3A%22b%22%2C%22c%22%3A%22d%22%7D"
const params = parseQueryString(url)

console.log(params)
