// 给定一个大小为 n 的数组 nums ，返回其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。

// 你可以假设数组是非空的，并且给定的数组总是存在多数元素。


// 示例 1：
// 输入：nums = [3,2,3]
// 输出：3

// 示例 2：
// 输入：nums = [2,2,1,1,1,2,2]
// 输出：2


/**
 * @param {number[]} nums
 * @return {number}
 */


//方法一
let num = [1]
let max = 0;

var majorityElement = function (nums) {
  const obj = {};
  for (let i of nums) {
    obj[i] = (obj[i] || 0) + 1;
  }
  for (let i in obj) {
    // i返回键
    max = Math.max(max, obj[i])
  }
  return findkey(obj, max)
};


var findkey = function (obj, value) {
  console.log(Object.keys(obj));
  return Object.keys(obj).find((key) => {
    return obj[key] == value
  })
}

console.log(majorityElement(num))



//上面的方法在leetcode只能ac 40%


//方法二

var majorityElement2 = function (nums) {
  const map = {}
  const n = nums.length >> 1 // >>是右移运算符，意思是除以2
  for (let i = 0; i < nums.length; i++) {
    map[nums[i]] = map[nums[i]] !== undefined ? map[nums[i]] + 1 : 1
    if (map[nums[i]] > n) return nums[i]
  }
}

//该方法全ac



