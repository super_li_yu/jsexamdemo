// 实现一个模板替换function，例如：输入aaa{{xxx}}bbb{{yyy}}ccc，{xxx:1,yyy:2}，返回aaa1bbb2ccc
let str = `aaa{{xxx}}bbb{{yyy}}ccc`
let obj = { xxx: 1, yyy: 2 }

const fn = (str, obj) => {
  let count = 0;
  let temp = '';
  let res = '';
  for (let i = 0; i < str.length; i++) {
    if (str[i] === '{') {
      count++;
    } else if (str[i] === '}') {
      count--;
      if (count === 0) {
        res += obj[temp];
        temp = '';
      }
    } else if (count == 2) {
      temp += str[i];
    } else {
      res += str[i];
    }
  }
  return res;
}

console.log(fn(str, obj));