/**
 * 387_给定一个字符串 s ，找到 它的第一个不重复的字符，并返回它的索引 。如果不存在，则返回 -1 。
 */

// 示例 1：
// 输入: s = "leetcode"
// 输出: 0

// 示例 2:
// 输入: s = "loveleetcode"
// 输出: 2


// 示例 3:
// 输入: s = "aabb"
// 输出: -1

// 提示:
// 1 <= s.length <= 105

var firstUniqChar = function (s) {
  let len = s.length;
  let map = {};
  for (let i of s) {
    map[i] = (map[i] || 0) + 1;
  }
  console.log(map);
  for (let i = 0; i < len; i++) {
    console.log(map[s[i]])
    if (map[s[i]] == 1) {
      return i;
    } else {
      continue;
    }
  }
  return -1
};

console.log(firstUniqChar("loveleetcode"))


