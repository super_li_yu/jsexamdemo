// 问题描述: 给定一个包含红色、白色和蓝色，一共 n 个元素的数组，原地对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排列。 
// 此题中，我们使用整数 0、1 和 2 分别表示红色、白色和蓝色。 

// 输入描述: 
// 1
// 输入：数组nums 

// 输出描述: 
// 1
// 输出：数组nums 

// 输入样例: 
// 1
// 2 0 2 1 1 0 

// 输出样例:
// 1
//  [0,0,1,1,2,2]


// Array.prototype.sort
let nums = [1, 2, 0, 0, 1, 2];
function handleArr(nums) {
  //不是数组直接返回
  if (!nums instanceof Array) {
    return nums;
  }
  //是数组进行处理
  return nums.sort();
}

console.log(handleArr(nums));