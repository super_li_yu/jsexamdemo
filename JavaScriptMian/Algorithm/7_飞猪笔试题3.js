/**
    * @description: 按照给定的属性值和顺序为数组排序
    * @param { Array } arr 需排序的原数组
    * @param { String } property 排序的依据属性
    * @param { Boolean } order 正序：true，反序：false，可选，默认为正序
    * @return { Array } 已排序的数组
*/
/*   实现方法请考虑通用性   */
/*   请注意，实现方法需符合纯函数要求    */


//-----做答
const sortBy = (arr, property, order = true) => {
  if (arr.length < 2) return arr
  let temp = [...arr]
  for (let i = 0; i < temp.length - 1; i++) {
    let [v, property] = [temp[i][property], i]
    for (let j = i + 1; j < temp.length; j++) {
      if (Number(v) > Number(temp[j][property])) {
        v = temp[j][property]
        pos = j
      }
    }
    [temp[i], temp[pos]] = [temp[pos], temp[i]]
  }
  return order ? temp : temp.reverse()
}

const arr = [
  { timestamp: 1606098032410, value: 1232, lable: 3 },
  { timestamp: 1606098032420, value: 532, lable: 1 },
  { timestamp: 1606098032440, value: 132, lable: 24 },
  { timestamp: 1606098032460, value: 432, lable: 13 },
  { timestamp: 1606098032470, value: 1812, lable: 5 },
  { timestamp: 1606098032480, value: 1932, lable: 8 },
  { timestamp: 1606098032490, value: 132, lable: 4 },
  { timestamp: 1606098032500, value: 2462, lable: 9 }
]

console.log(JSON.stringify(sortBy(arr, 'lable', false))) 