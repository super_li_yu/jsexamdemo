# 面试题

## 浏览器
### 1.说一下浏览器的缓存
浏览器两类：协商缓存和强制缓存
[链接](http://t.csdn.cn/rHBsO)

### 2. 说一下把url输入到地址栏到加载出页面的过程

1. 把url输入地址栏中

2. 浏览器先查找当前url是否存在缓存，并比较缓存是否过期

  - 未过期的话，资源在本地磁盘或者内存里就都有了，直接加载可以节省很多时间。
  - 检验新鲜通常由两个HTTP头进行控制`Expires`和`Cache-Control`
    - HTTP1.0通常提供Expres,值为一个绝对时间表示缓存新鲜日期。
    - HTTP1.1增加了Cache-Control:max-age= ,值为以秒为单位的最大新鲜时间。

3. 浏览器解析URL获取协议，主机，端口，path

4. 浏览器组装一个HTTP（GET）请求报文。

5. 浏览器获取主机的ip地址（先访问本机，后找DNS域名解析器帮助）

  - 浏览器缓存
  - 本机缓存
  - hosts文件
  - 路由器缓存
  - ISP DNS缓存
  - DNS递归查询

6. 打开一个socket和目标IP地址端口建立TCP链接，三次握手如下。

  - 客户端发送一个TCP的SYN=1,Seq=x的包到服务器端口
  - 服务器发回SYN=1,ACK=X+1,Seq=Y的响应包
  - 客户端发送ACK=Y+1,Seq=Z

7. TCP链建立后，发送HTTP请求

8. 服务器接受请求并进行解析，将请求转发到服务程序，如虚拟主机使用HTTP Host头部判断请求的服务程序。

9. 服务器检查HTTP请求头是否包含缓存信息，如果缓存新鲜，返回304等对应的状态码。
10. 处理程序读取完整请求并准备HTTP响应，可能需要查询数据库等操作。
11. 服务器将响应报文通过TCP连接发送回浏览器
12. 浏览器接受HTTP响应，然后根据情况选择关闭TCP连接或者保留重用，关闭TCP连接的四次握手如下：
      - 主动方发送**Fin=1， Ack=Z， Seq= X**报文
      - 被动方发送**ACK=X+1， Seq=Z**报文
      - 被动方发送**Fin=1， ACK=X， Seq=Y**报文
      - 主动方发送**ACK=Y， Seq=X**报文

  ### 3.url输入到地址栏到展示页面用到的缓存

![从输入URL到展示页面涉及哪些缓存环节](https://super-ly-image.oss-cn-hangzhou.aliyuncs.com/%E4%BB%8E%E8%BE%93%E5%85%A5URL%E5%88%B0%E5%B1%95%E7%A4%BA%E9%A1%B5%E9%9D%A2%E6%B6%89%E5%8F%8A%E5%93%AA%E4%BA%9B%E7%BC%93%E5%AD%98%E7%8E%AF%E8%8A%82.png)

### 4.前端需要注意哪些SEO

SEO就是利于爬虫抓取页面的，我们可以从`title`,`descript`,`keywords`来进行优化。

- 合理的`Title`
- 明确的`descript`
- 正确的`keywords`
- 语义化的`HTML`代码：aside,content这些
- 重要内容不用`js输出`。
- 使用服务端渲染。不要用SPA单页面应用。



## HTML

### 1. Html5的新特性有哪些

- 语义化标签
- 增强型表单
- 视频和音频标签
- Canvas绘图
- SVG绘图
- WebStorage
- WebSocket
- WebWorker：是运行在后台的JavaScript，独立于其他脚本，不会影响页面的性能。

### 2. 说一下css的盒模型

![css盒模型](https://super-ly-image.oss-cn-hangzhou.aliyuncs.com/css%E7%9B%92%E6%A8%A1%E5%9E%8B.png)

### 3.`<img>`的`title`和`alt`有什么区别

- title：通常是当鼠标滑动到元素上的时候显示
- alt：是`<img>`的特有属性，是图片内容的等价描述，用于图片无法加载时显示。

## CSS

### 1. 说一下如何实现水平垂直居中

```html
<div class = 'father'>
    <div class = 'son'>
        居中元素
    </div>
</div>
```



```css
方法一:定位+margin

.father{
    position:relative;
    
}

.son{
    position:absolute;
    top:0;
    left:0;
    right:0;
    bottom:0;
    margin:auto;
}


方法二:定位+transform
.father{
    position:relative;
}

.son{
    position:absolute;
    top:50%;
    right:50%;
    transform:translate(-50%,-50%);     
}

方法三:fiex
.father{
    display:flex;
	justify-content: center;
    align-items: center;
}

.son{ 
}


方法四:grid
.father{
    display:grid;
	justify-content: center;
    align-items: center;
}

.son{ 
}
```



## Js

### 1.说一下为什么0.1+0.2 != 0.3

这就要说一下JavaScript存储数据的方式了，JavaScript存储数据是使用IEEE754标准，也就是说JavaScript所有的数字都保存成64位浮点数(双精度浮点数)，这个数值的精度只能到53个二进制位(相当于16个十进制位)，大于这这个范围的整数，JavaScript是无法精确表示的。同时大于或者等于2的1024次方的数值，java无法表示。

### 2.简单说一下你对promise的理解

`Promise是一门是在ES6里提出的新技术，是JS在异步编程的一种解决方案，可以支持链式调用，可以解决地狱问题指定回调的问题更加灵活。`

![promise](https://super-ly-image.oss-cn-hangzhou.aliyuncs.com/promise.png)

- `Promise` 是 `ES6` 新增的语法，解决了回调地狱的问题。
- 可以把 `Promise`看成一个状态机。初始是 `pending` 状态，可以通过函数 `resolve` 和 `reject`，将状态转变为 `resolved` 或者 `rejected` 状态，状态一旦改变就不能再次变化。
- `then` 函数会返回一个 `Promise` 实例，并且该返回值是一个新的实例而不是之前的实例。因为 `Promise` 规范规定除了 `pending` 状态，其他状态是不可以改变的，如果返回的是一个相同实例的话，多个 `then` 调用就失去意义了。 对于 `then` 来说，本质上可以把它看成是 `flatMap`

#### 1.Promise的基本情况

> 简单来说它就是一个容器，里面保存着某个未来才会结束的时间，通常是异步操作的结果，从它可以获取异步操作的消息。

一般 Promise 在执行过程中，必然会处于以下几种状态之一。

- 待定（`pending`）：初始状态，既没有被完成，也没有被拒绝。
- 已完成（`fulfilled`）：操作成功完成。
- 已拒绝（`rejected`）：操作失败。

> 待定状态的 `Promise` 对象执行的话，最后要么会通过一个值完成，要么会通过一个原因被拒绝。当其中一种情况发生时，我们用 `Promise` 的 `then` 方法排列起来的相关处理程序就会被调用。因为最后 `Promise.prototype.then` 和 `Promise.prototype.catch` 方法返回的是一个 `Promise`， 所以它们可以继续被链式调用

关于 Promise 的状态流转情况，有一点值得注意的是，内部状态改变之后不可逆，你需要在编程过程中加以注意。文字描述比较晦涩，我们直接通过一张图就能很清晰地看出 Promise 内部状态流转的情况

![Promise](https://s.poetries.work/images/20210414175500.png)



#### 5.Promise的静态方法

- all方法
  - 语法：Promise.all(iterable)
  - 参数：一个可以迭代的对象，如Array
  - 描述：此方法对于汇总多个Proimse的结果很有用，在ES6中可以将等多个Promise.all异步请求并行操作，返回结果一般有下面两种情况。
    - 当所有结果返回时按照请求顺序返回成功结果。
    - 当其中的一个方法失败时，则进入失败方法。



### 3. 如何实现一个变量的监听

1. 在Vue2.0中，变量的监听是通过`Object.definedprototype`来实现的
2. 在Vue3.0中，变量的监听是通过`Proxy`实现的。

### 4. proxy

Proxy 用于修改某些操作的默认行为，等同于在语言层面做出修改，所以属于一种“元编程”（meta programming），即对编程语言进行编程。

Proxy 可以理解成，在目标对象之前架设一层“`拦截`”，外界对该对象的访问，都必须先通过这层拦截，因此提供了一种机制，可以对外界的访问进行过滤和改写。Proxy 这个词的原意是代理，用在这里表示由它来“代理”某些操作，可以译为“代理器”。

```js
var boj = {
    get:funcation(target,propKey,receive){
    	console.log(`getting ${propKey}`);
		return  Reflect.get(target,propKey,reciever); 
    }
	set:funcation(target,propKey,value,revevver){
		console.log(`setting ${propKey}!`);
		return Reflet.set(target,propKey,value,receiver);
	}
}
```

### 5. 那些操作会造成内存泄漏？

> JavaScript 内存泄露指对象在不需要使用它时仍然存在，导致占用的内存不能使用或回收

- 未使用 var 声明的全局变量
- 闭包函数(Closures)
- 循环引用(两个对象相互引用)
- 控制台日志(console.log)
- 移除存在绑定事件的DOM元素(IE)
- `setTimeout` 的第一个参数使用字符串而非函数的话，会引发内存泄漏
- 垃圾回收器定期扫描对象，并计算引用了每个对象的其他对象的数量。如果一个对象的引用数量为 0（没有其他对象引用过该对象），或对该对象的惟一引用是循环的，那么该对象的内存即可回收



### 6. XML和JSON的区别

- 数据体积方面
  - `JSON`相对`于XML`来讲，数据的体积小，传递的速度更快些。
- 数据交互方面
  - `JSON`与`JavaScript`的交互更加方便，更容易解析处理，更好的数据交互
- 数据描述方面
  - `JSON`对数据的描述性比`XML`较差
- 传输速度方面
  - `JSON`的速度要远远快于`XML`

### 7.常见的web安全以及防护原理

 

- `sql`注入原理
  - 就是通过把`SQL`命令插入到`Web`表单递交或输入域名或页面请求的查询字符串，最终达到欺骗服务器执行恶意的SQL命令
- 总的来说有以下几点
  - 永远不要信任用户的输入，要对用户的输入进行校验，可以通过正则表达式，或限制长度，对单引号和双`"-"`进行转换等
  - 永远不要使用动态拼装SQL，可以使用参数化的`SQL`或者直接使用存储过程进行数据查询存取
  - 永远不要使用管理员权限的数据库连接，为每个应用使用单独的权限有限的数据库连接
  - 不要把机密信息明文存放，请加密或者`hash`掉密码和敏感的信息

**XSS原理及防范**

- `Xss(cross-site scripting)`攻击指的是攻击者往`Web`页面里插入恶意`html`标签或者`javascript`代码。比如：攻击者在论坛中放一个看似安全的链接，骗取用户点击后，窃取`cookie`中的用户私密信息；或者攻击者在论坛中加一个恶意表单，当用户提交表单的时候，却把信息传送到攻击者的服务器中，而不是用户原本以为的信任站点

**XSS防范方法**

- 首先代码里对用户输入的地方和变量都需要仔细检查长度和对`”<”,”>”,”;”,”’”`等字符做过滤；其次任何内容写到页面之前都必须加以encode，避免不小心把`html tag` 弄出来。这一个层面做好，至少可以堵住超过一半的XSS 攻击

**XSS与CSRF有什么区别吗？**

- `XSS`是获取信息，不需要提前知道其他用户页面的代码和数据包。`CSRF`是代替用户完成指定的动作，需要知道其他用户页面的代码和数据包。要完成一次`CSRF`攻击，受害者必须依次完成两个步骤
- 登录受信任网站`A`，并在本地生成`Cookie`
- 在不登出`A`的情况下，访问危险网站`B`

**CSRF的防御**

- 服务端的`CSRF`方式方法很多样，但总的思想都是一致的，就是在客户端页面增加伪随机数
- 通过添加验证码的方法

### 8. 什么要有同源策略

- 同源策略指的是：协议，域名，端口相同，同源策略是一种安全协议
- 举例说明：比如一个黑客程序，他利用`Iframe`把真正的银行登录页面嵌到他的页面上，当你使用真实的用户名，密码登录时，他的页面就可以通过`Javascript`读取到你的表单中`input`中的内容，这样用户名，密码就轻松到手了。

### 9.offsetWidth/offsetHeight,clientWidth/clientHeight与scrollWidth/scrollHeight的区别

- `offsetWidth/offsetHeight`返回值包含**content + padding + border**，效果与e.getBoundingClientRect()相同
- `clientWidth/clientHeight`返回值只包含**content + padding**，如果有滚动条，也**不包含滚动条**
- `scrollWidth/scrollHeight`返回值包含**content + padding + 溢出内容的尺寸**

### 10.Js中如何定义对象

- 对象字面量： `var obj = {};`
- 构造函数： `var obj = new Object();`
- Object.create(): `var obj = Object.create(Object.prototype);`

### 11. JS的基本数据类型和引用数据类型

- 基本数据类型：`undefined`、`null`、`boolean`、`number`、`string`、`symbol`
- 引用数据类型：`object`、`array`、`function`

### 12.Js有哪些内置对象

- `Object` 是 `JavaScript` 中所有对象的父对象
- 数据封装类对象：`Object`、`Array`、`Boolean`、`Number` 和 `String`
- 其他对象：`Function`、`Arguments`、`Math`、`Date`、`RegExp`、`Error`

### 13.Js中有几种类型的值

- 栈：原始数据类型（`Undefined`，`Null`，`Boolean`，`Number`、`String`）
- 堆：引用数据类型（对象、数组和函数）
- 两种类型的区别是：存储位置不同；
- 原始数据类型直接存储在栈(`stack`)中的简单数据段，占据空间小、大小固定，属于被频繁使用数据，所以放入栈中存储；
- 引用数据类型存储在堆(`heap`)中的对象,占据空间大、大小不固定,如果存储在栈中，将会影响程序运行的性能；引用数据类型在栈中存储了指针，该指针指向堆中该实体的起始地址。当解释器寻找引用值时，会首先检索其
- 在栈中的地址，取得地址后从堆中获得实体

### 14. eval的作用

- 它的功能是把对应的字符串解析成`JS`代码并运行
- 应该避免使用`eval`，不安全，非常耗性能（`2`次，一次解析成`js`语句，一次执行）
- 由`JSON`字符串转换为JSON对象的时候可以用`eval，var obj =eval('('+ str +')')`

### 15.null,undefined的区别

- `undefined` 表示不存在这个值。
- `undefined` :是一个表示"无"的原始值或者说表示"缺少值"，就是此处应该有一个值，但是还没有定义。当尝试读取时会返回 `undefined`
- 例如变量被声明了，但没有赋值时，就等于`undefined`
- `null` 表示一个对象被定义了，值为“空值”
- `null` : 是一个对象(空对象, 没有任何属性和方法)
- 例如作为函数的参数，表示该函数的参数不是对象；
- 在验证`null`时，一定要使用　`===` ，因为 `==`无法分别`null` 和　`undefined`

### 16. null为什么是对象

null有时会被当作一种对象类型，即对null执行typeof null是，会返回`object`

> 原理是这样的，不同的对象在底层都表现为二进制，在JavaScript中，二进制前3位入宫为0的话，就会被判断为Object类型，null的二进制表示是全0，自然前三位也是0，所以执行`typeOf`是会返回"object".

### 17. 如何使用JS判断一个数组

- instanceof方法

  - `instanceof` 运算符是用来测试一个对象是否在其原型链原型构造函数的属性

```javascript
var arr = [];
arr instanceof Array; // true   
```

- constructor方法

  - `constructor`属性返回对创建此对象的数组函数的引用，就是返回对象相对应的构造函数

```javascript
var arr = [];
arr.constructor == Array; //true
    
```

- 最简单的方法
  - 这种写法，是 `jQuery` 正在使用的

```javascript
Object.prototype.toString.call(value) == '[object Array]'
// 利用这个方法，可以写一个返回数据类型的方法
var isType = function (obj) {
     return Object.prototype.toString.call(obj).slice(8,-1);
}
```

- `ES5`新增方法`isArray()`

```javascript
var a = new Array(123);
var b = new Date();
console.log(Array.isArray(a)); //true
console.log(Array.isArray(b)); //false
```

### 18. map和forEach的区别

#### 相同点

- 循环遍历数组中的每一项。
- 每次执行匿名函数都支持三个参数:(item(当前每一项),index(🔒引值),arr(原数组))
- 只能遍历数组

#### 不同点

- map()会分配内存空间存储新数组返回，forEach不会返回新数组。
- forEach()运行callback更改原始数组的元素，map()返回新的数组，map()不会对空数组进行检测。
- forEach()方法没有返回值，而map()方法有返回值。



## Vue

### 1.nextTick

- nextTick()是等待下一次DOM更新刷新的工具方法，可以在状态改变后立即使用，以等待DOM更新完成。你可以传递一个回调函数作为参数，或者await中的Promise。
- Vue中如果书变化，Vue不会立即更新DOM，而是开启一个队列，把组件更新函数保存到队列中，等同一个事件循环发生的所有数据变更完成后，再统一对视图进行更新。此时入果想要获取更新后的DOM状态，就可以使用nextTick。
- 使用场景：在created或setup中想要获取DOM时，在响应式数据变化后获取DOM更新后的状态时。



## 跨域

![跨域](https://pic3.zhimg.com/v2-0d3c47d34c6a5e947013215ec6338a06_r.jpg)

### 1.Jsonp跨域

#### 什么是JSONP

`jsonp`是民间提出的一种跨域解决方案，通过客户端的`script标签发送请求方式`。

- 原理就是通过添加一个`<script>`标签，向服务器请求Json数据，这样就不受同源策略的限制，服务器接收到请求后，将数据放在一个callback({返回数据})传回来，比如axios。不过只支持GET请求，而且不安全，可能遇到XSS攻击，不过它的好处就是向老浏览器或不支持CORS的网址请求数据。
- 服务器：将服务端返回数据封装到指定的函数中返回callback({返回数据})
- 客户端：不管是我们的script标签，还是src还是img标签的src，或者说link标签的href，他们没有被同源策略所限制，比如我们有时候可能使用一个网络上的图片，就可以请求得到：所以利用同源策略的漏洞，将访问地址放在下面的标签路径中。

```html
<script src = "www.baodu.com"></script>, <img src=""/> ,<link href=""/>
```

来解决跨域问题。

#### JSONP原理

![JSON原理](https://img-blog.csdnimg.cn/1acd06fa155a4463a8c868884955b6ff.png#pic_center)

#### JSOINP的实现

`<img src = xxx>`

`<link href = xxx>`

`<script src = xxx>`

- 获取客户端发送过来的回调参数的名字
- 得到要通过JSONP形式发送给客户端的数据
- 根据前两部得到的数据，拼接一个函数调用的字符串
- 把上一步拼接得到的字符串，响应给客户端的`<script>`标签进行解析执行。

```js
   const express = require('express')
   const app = express()
   const port = 3000

   //路由配置
   app.get("/user",(req,res)=>{
      //1.获取客户端发送过来的回调函数的名字
      let fnName = req.query.callback;
      //2.得到要通过JSONP形式发送给客户端的数据
      const data = {name:'tom'}
      //3.根据前两步得到的数据，拼接出个函数调用的字符串
      let result = `${fnName}({name:"tom"})`
      //4.把上步拼接得到的字符串，响应给客户端的<script> 标签进行解析执行
      res.send(result);
   })

   app.listen(port, () => {
      console.log(`Example app listening on port ${port}`)
   })

```

```js
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>JSONP简单实现</title>
   </head>
      <body>
         <button id="btn">点击发送请求</button>
         <script>
            function getJsonpData(data) {
               console.log("获取数据成功")
               console.log(data) //{name:'tom'}
            }
            var btn = document.getElementById("btn");
            btn.onclick = function () {
               //创建script标签
               var script = document.createElement("script");
               script.src = 'http://localhost:3000/user?callback=getJsonpData';
               document.body.appendChild(script);
               script.onload = function () {
                  document.body.removeChild(script)
               }
            }
         </script>
      </body>
   </html>

```

### 2.nginx反向代理

反向代理和代理很像，都是再服务器和服务器中间假设一个中间代理服务器，不同点在于代理是客户端发送请求，反向代理是代表服务端接受客户端发送的请求。

![反向代理](https://img-blog.csdnimg.cn/c3a7336752e54566b9e0dc9007fc7a9d.png)

#### 原理

![反向代理](https://super-ly-image.oss-cn-hangzhou.aliyuncs.com/%E5%8F%8D%E5%90%91%E4%BB%A3%E7%90%86.png)

### 3.代理转发

Vue就是使用的代理转发，在vue中我们可以在vue.confit.js中配置代理服务器。代理服务器和客户端同源，而服务器和服务器之间的网络请求并不会收到浏览器的拦截，所以这也是此方法可行的原因。

![代理转发](https://super-ly-image.oss-cn-hangzhou.aliyuncs.com/%E4%BB%A3%E7%90%86%E8%BD%AC%E5%8F%91.png)

### 4.document.domain + iframe

> 注意：只能在`主域相同的时候`，才能使用该方法。

- 父窗口：(http://www.domain.com/a.html)

```html
<iframe id="iframe" src="http://child.domain.com/b.html"></iframe>
<script>
    document.domain = 'domain.com';
    var user = 'admin';
</script>
```

- 子窗口：(http://child.domain.com/b.html)

```js
document.domain = 'domain.com';
// 获取父窗口中变量
alert('get js data from parent ---> ' + window.parent.user);
```



