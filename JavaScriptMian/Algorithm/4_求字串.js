/**
 * 输入一个数组，数组的每一项均为0-10的整数，设计一个函数，输出此数组中和为10的元素总和
 */

str1 = [1, 2, 3, 4, 10];

function add(num, n) {
  let res = [];
  let sum = n;
  for (let i in num) {
    if (i == 10) {
      res.push(i);
      break;
    } else {
      for (let j = i + 1; j < num.length; i++) {
        if (num[j] + sum[i] == 10) {
          res.push(num[j])
        } else {
          add(num, n - 1);
        }
      }
    }
  }
  return res;
}