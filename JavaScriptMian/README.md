# JavaScript练手小案例

:hamburger:钻研问题，熟悉常用方法。

:jack_o_lantern::加深理解，学会编程思想。

:tada:多次复习，实现代码复用。



## 介绍
该仓库用作本人学习一些JavaScript-API的仓库，里面可能会包含各种常考的api的使用案例，以及仓靠的一些编程题。



## 目录

### Algorithm

一些和`JavaScript`相关的算法题

### JavaScript

主要为`JavaScript`中一些常用API的小例子

### JsBasics

面试常考的算法题，比如防抖，节流，深浅拷贝等。

### TypeScript

`JavaScript`的超集，里面存放了`TypeScript`相关的小例子。