# WebExamDemo

该仓库用作本人学习一些JavaScript-API和Html+CSS布局的仓库，里面可能会包含各种常考的api的使用案例以及布局案例，以及常考的一些编程题。

# 目录
`HtmlCss_main`：html+css项目案例布局仓库
`JavaScript_main`：javaScript以及算法相关案例仓库


> 仅为记录个人学习，也不排斥各位大佬参观！
> 仓库设置为公开是希望能够帮助到需要帮助的小伙伴让大家一起在前端的路上走的更久。